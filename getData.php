<?php

function getGroups($conn): array
{
    $sql = "SELECT * FROM `groups`";
    $result = $conn->query($sql);
    $groups = array();
    while ($row = $result->fetch_assoc()) {
        $groups[$row["id"]] = $row["name"];
    }

    return $groups;
}

function getGenders($conn): array
{
    $sql = "SELECT * FROM `genders`";
    $result = $conn->query($sql);
    $genders = array();
    while ($row = $result->fetch_assoc()) {
        $genders[$row["id"]] = $row["name"];
    }

    return $genders;
}