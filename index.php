<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="styles.css">
    <title>My PWA</title>
    <link rel="manifest" href="manifest.json">
</head>
<body>
<?php include("config.php") ?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample"
                    aria-controls="offcanvasExample" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample"
                 aria-labelledby="offcanvasExampleLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasExampleLabel">Меню</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <a href="index.php" class="list-group-item list-group-item-action border-0 mb-2">Dashboard</a>
                    <a href="index.php"
                       class="list-group-item list-group-item-action border-0 mb-2 active">Students</a>
                    <a href="index.php" class="list-group-item list-group-item-action border-0 mb-2">Tasks</a>
                </div>
            </div>
        </div>
        <a class="navbar-brand ms-3" href="#">CMS</a>
        <div class="ms-auto">
            <div class="btn-group">
                <div class="dropdown">
                    <a href="register.html" class="btn dropdown text-white btn-lg bell-blink">
                        <i class="bi bi-bell"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-center" aria-labelledby="dropdownMenuButton1">
                        <li>
                            <a class="dropdown-item" href="#">
                                <div class="message-container">
                                    <div class="message-container">
                                        <div class="avatar-container">
                                            <i class="bi bi-person-circle avatar-icon"></i>
                                            <p class="m-0">Admin</p>
                                        </div>

                                        <div class="message-text">
                                            <div class="sender-triangle"></div>
                                            Привіт! Як справи?
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="btn-group">
                <div class="dropdown">
                    <button class="btn text-white btn-lg" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span><img src="avatar.jpg" alt="Аватарка" class="avatar"></span>
                        <span class="d-none d-sm-inline">James Bond</span>
                    </button>

                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Log Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="container-m5 me-3 table-container ms-1">
    <div class="row">
        <div class="col-3 d-md-block d-none">
            <div>
                <a href="index.php" class="list-group-item list-group-item-action border-0 mb-2">Dashboard</a>
                <a href="index.php" class="list-group-item list-group-item-action border-0 active mb-2">Students</a>
                <a href="index.php" class="list-group-item list-group-item-action border-0 mb-2">Tasks</a>
            </div>
        </div>
        <div class="col-md-9 ml-md-0 ml-3">
            <h1>Students</h1>
            <div class="d-flex flex-row-reverse">


                <button class="icon-btn btn btn-outline-secondary btn-sm editStudentBtn" data-id=""><i
                            class="bi bi-plus-lg"></i></button>
            </div>
            <div class="table-responsive-md">


                <table class="mt-3 table table-bordered text-center" id="studentTable">
                    <thead>
                    <tr>
                        <th scope="col"><input class="custom-checkbox3" type="checkbox"></th>
                        <th scope="col">Group</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Status</th>
                        <th scope="col">Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while ($row = $result->fetch_assoc()) {
                        ?>
                        <tr data-student-id="<?php echo $row['id']; ?>">
                            <td><input class="custom-checkbox3" type="checkbox"></td>
                            <td><?php echo $row['group_name']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo substr($row['gender_name'], 0, 1); ?></td>
                            <td><?php echo $row['birthday']; ?></td>
                            <td class="align-middle">
                                <div class="circle <?php echo ($row['status'] === "1" ? "active" : ""); ?>"></div>
                            </td>
                            <td>
                                <div class="d-flex justify-content-center align-items-center actionIcons">
                                    <div class="d-flex justify-content-center align-items-center actionIcons">
                                        <button class="icon-btn btn btn-outline-secondary btn-sm deleteStudentBtn" data-id="<?php echo $row['id']; ?>">
                                            <i class="bi bi-x"></i>
                                        </button>
                                        <button class="icon-btn btn btn-outline-secondary btn-sm editStudentBtn" data-id="<?php echo $row['id']; ?>">
                                            <i class="bi bi-pencil"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center mt-5">
        <li class="page-item"><a class="page-link" href="#"><</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item"><a class="page-link" href="#">></a></li>
    </ul>
</nav>


<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="studentModalLabel">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">WARNING</h5>
                <button type="button" class="btn btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="studentIdDel">
                <p id="deleteUserMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmDeleteBtn" class="icon-btn btn btn-outline-secondary btn-sm">Ok
                </button>
                <button type="button" class="icon-btn btn btn-outline-secondary btn-sm" data-bs-dismiss="modal">Cancel
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="studentModal" tabindex="-1" role="dialog" aria-labelledby="studentModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="studentModalLabel">Student Details</h5>
                <button type="button" class="btn btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="studentId">
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">Group</label>
                    </div>
                    <div class="col-md-6">
                        <select class="form-select form-control" id="group">
                            <option value="" disabled selected>Select group</option>
                            <?php foreach ($groups as $key => $value): ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">First name</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="firstName">
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">Last name</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="lastName">
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">Gender</label>
                    </div>
                    <div class="col-md-6">
                        <select class="form-select form-control" id="gender">
                            <option value="" disabled selected>Select gender</option>
                            <?php foreach ($genders as $key => $gender) { ?>
                                <option value="<?= $key ?>"><?= $gender ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">Birthday</label>
                    </div>
                    <div class="col-md-6">
                        <input type="date" class="form-control" id="birthday">
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <label class="form-label text-start">Status</label>
                    </div>
                    <div class="col-md-6">
                        <input type="checkbox" class="form-check-input" id="status">
                        <label class="form-check-label" for="status">Active</label>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="saveStudentBtn" class="btn btn-primary">OK</button>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="app.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', function () {
            navigator.serviceWorker.register('sw.js').then(function (registration) {
                console.log('ServiceWorker registration successful with scope: ', registration.scope);
            }, function (err) {
                console.log('ServiceWorker registration failed: ', err);
            });
        });
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>
</body>
</html>
