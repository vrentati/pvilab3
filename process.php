<?php

function handleRequest(): void
{
    include 'config.php';
    header('Content-Type: application/json');
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (isset($_POST["deleteStudent"])) {
            deleteStudent($conn);
        } elseif (isset($_POST["confirmStudent"])) {
            confirmStudent($conn);
        } else {
            // Якщо передано невідомий параметр
            $response = array();
            $response["success"] = false;
            $response["message"] = "Unknown parameter.";
            echo json_encode($response);
        }
    } else {
        // Якщо запит не є POST
        $response = array();
        $response["success"] = false;
        $response["message"] = "Invalid request method.";
        echo json_encode($response);
    }
}

function deleteStudent($conn): void {
    // Екранування параметра
    $studentId = mysqli_real_escape_string($conn, $_POST["studentId"]);

    // Підготовка запиту з параметрами
    $sql = "DELETE FROM students WHERE id = ?";
    $stmt = $conn->prepare($sql);

    $stmt->bind_param("i", $studentId);

    // Виконання підготовленого запиту
    if ($stmt->execute()) {
        // Формування відповіді
        $response = array();
        $response["success"] = true;
        $response["message"] = "Student deleted successfully.";
    } else {
        $response["success"] = false;
        $response["message"] = "Error while deleting student from db";
    }

    // Закриття підготовленого запиту
    $stmt->close();

    // Вивід відповіді у форматі JSON
    echo json_encode($response);
}


function findStudentById($id, $conn) {
    $query = "SELECT * FROM students WHERE id = '$id'";
    return $conn->query($query)->fetch_assoc();
}

function addOrUpdateStudent($group, $firstName, $lastName, $gender, $birthday, $status, $id, $conn) {
    // Екранування параметрів
    $group = mysqli_real_escape_string($conn, $group);
    $firstName = mysqli_real_escape_string($conn, $firstName);
    $lastName = mysqli_real_escape_string($conn, $lastName);
    $gender = mysqli_real_escape_string($conn, $gender);
    $birthday = mysqli_real_escape_string($conn, $birthday);
    $status = mysqli_real_escape_string($conn, $status);
    $id = mysqli_real_escape_string($conn, $id);

    if (empty($id)) {
        // Додавання нового студента
        $sql = "INSERT INTO students (group_id, firstname, lastname, gender_id, birthday, status) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("isssss", $group, $firstName, $lastName, $gender, $birthday, $status);
    } else {
        // Оновлення існуючого студента
        $sql = "UPDATE students SET group_id = ?, firstname = ?, lastname = ?, gender_id = ?, birthday = ?, status = ? WHERE id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("isssssi", $group, $firstName, $lastName, $gender, $birthday, $status, $id);
    }

    $result = $stmt->execute();

    // Закриття підготовленого запиту
    $stmt->close();

    return $result;
}



function confirmStudent($conn): void
{
    // Отримання даних з POST запиту
    $group = $_POST["group"];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $gender = $_POST["gender"];
    $birthday = $_POST["birthday"];
    $status = $_POST["status"];
    $id = $_POST["id"];

    // Перевірка на коректність даних
    $response = array();
    $response["errors"] = array();

    // Перевірка на пустоту полів
    if (empty($group)) {
        $response["errors"]["group"] = "Group field is required.";
    }
    if (empty($firstName)) {
        $response["errors"]["firstName"] = "First name field is required.";
    }
    if (empty($lastName)) {
        $response["errors"]["lastName"] = "Last name field is required.";
    }
    if (empty($gender)) {
        $response["errors"]["gender"] = "Gender field is required.";
    }
    if (empty($birthday)) {
        $response["errors"]["birthday"] = "Birthday field is required.";
    }


    if (empty($response["errors"])) {
        // Дані вірні, підготовка відповіді
        $response["success"] = true;
        $response["message"] = "Data is correct. Student added/updated successfully.";
        $response["data"] = array(
            "group" => $group,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "gender" => $gender,
            "birthday" => $birthday,
            "status" => $status
        );
        if (addOrUpdateStudent($group, $firstName, $lastName, $gender, $birthday, $status, $id, $conn)) {
            global $conn;
            if (empty($id)) {
                $response["id"] = $conn->insert_id; // Отримати ID нового студента
            } else {
                $response["id"] = $id; // Використовувати існуючий ID
            }
        }
    } else {
        // Якщо дані не коректні, встановлення success в false
        $response["success"] = false;
    }

    // Відправлення відповіді у форматі JSON

    echo json_encode($response);
}


// Виклик функції для обробки запиту
handleRequest();

